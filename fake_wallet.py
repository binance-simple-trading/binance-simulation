from typing import Dict, Tuple
from copy import deepcopy
from simple_logger import dlog

class FakeWallet:
    def __init__(self, initPrices:Dict[str, float], initBalance:Dict[str, Tuple[float]]):
        self.initBalance = initBalance
        self.initPrices = initPrices

        for key in initBalance:
            assert len(initBalance[key]) == 2, "Expected a tuple of two items (free and in use) for coin %s" % key
        self.balance = deepcopy(initBalance)
        self.prices = deepcopy(initPrices)

    # Extracts count1 from coin1 and adds count2 to coin2
    def updateBalance(self, coin1, coin2, count1, count2):
        assert count1 > 0 and count2 > 0
        assert coin1 in self.balance

        freeCoin1, inUseCoin1 = self.balance[coin1]
        freeCoin2, inUseCoin2 = self.balance[coin2] if coin2 in self.balance else (0.0, 0.0)
        assert count1 <= freeCoin1, "Coin 1 (%s) remains negative after balance update (%s vs %s)" \
            % (coin1, freeCoin1, count1)

        newFreeCoin1 = freeCoin1 - count1
        newFreeCoin2 = freeCoin2 + count2
        self.balance[coin1] = (newFreeCoin1, inUseCoin1)
        self.balance[coin2] = (newFreeCoin2, inUseCoin2)
        dlog(2, "[FakeWallet::updateBalance] Coin1: %s (-%2.5f). Coin2: %s (+%2.5f)" % (coin1, count1, coin2, count2))

        if newFreeCoin1 == 0 and inUseCoin1 == 0:
            dlog(2, "[FakeWallet::updateBalance] Removing coin %s" % coin1)
            del self.balance[coin1]

    def printSummary(self, showCoins:bool=False):
        coins = list(self.balance.keys())
        busdCoins = self.getBUSDValue()
        # values = map(lambda x : x[2], balance.values())
        total = sum(list(busdCoins.values()))
        dlog(1, "[FakeWallet::printSummary] Bot has %2.2f dollars in %d coin%s" % \
            (total, len(coins), "s" if len(coins) > 1 else ""))
        if showCoins:
            for coin in self.balance:
                free, inUse = self.balance[coin]
                busdValue = busdCoins[coin]
                dlog(1, " - Coin: %s. Free: %2.5f. In use: %2.5f. BUSD value: %2.2f" % (coin, free, inUse, busdValue))

    def updateBUSD(self, prices):
        self.prices = prices

    def getBUSDValue(self):
        assert not self.prices is None
        result = {}
        for coin in self.balance:
            free, inUse = self.balance[coin]
            unitBusd = self.prices["%sBUSD" % coin]
            result[coin] = (free + inUse) * unitBusd
        return result
