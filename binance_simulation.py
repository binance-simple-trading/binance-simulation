import time
import matplotlib.pyplot as plt
from binance_requests import BinanceClient
from binance_bot import BinanceBot
from simple_logger import dlog
from fake_wallet import FakeWallet
from typing import Optional

class BinanceSimulation:
    def __init__(self, bot, lag:float, endTime:int=1<<31, plotFilePath:str=None):
        self.client = BinanceClient(secretFilePath=None)
        self.bot = bot
        self.timestep = 0
        self.lag = lag
        self.endTime = endTime

        balance = dict(map(lambda x : (x[0], x[1][0:2]), self.bot.client.getAccountBalance()["balance"].items()))
        prices = self.client.getAllPrices()
        self.fakeWallet = FakeWallet(initPrices=prices, initBalance=balance)
        self.plotFilePath = plotFilePath

    def doAction(self, action, prices):
        dlog(1, "[BinanceSimulation] Action %s" % str(action))
        actionType, coin1, coin2, count, price = action
        symbol = "%s%s" % (coin1, coin2)
        reverseSymbol = "%s%s" % (coin2, coin1)
        assert actionType in ("BUY", "SELL", "PASS")
        if actionType == "PASS":
            return
        
        assert price == None, "Only market prices allowed for now."
        if not symbol in prices:
            assert reverseSymbol in prices, "Symbol %s not found in prices!" % symbol
            prices[symbol] = 1 / prices[reverseSymbol]
        if not reverseSymbol in prices:
            prices[reverseSymbol] = 1 / prices[symbol]
        assert ("%sBUSD" % coin1) in prices, "Coin %s has no BUSD translation. Not supported." % coin1
        assert ("%sBUSD" % coin2) in prices, "Coin %s has no BUSD translation. Not supported." % coin2
        unitPrice = prices[symbol]
        countCoin2 = count * prices[symbol]

        if actionType == "SELL":
            self.fakeWallet.updateBalance(coin1, coin2, count, countCoin2)
        elif actionType == "BUY":
            self.fakeWallet.updateBalance(coin2, coin1, countCoin2, count)

    def run(self):
        dlog(1, "[BinanceSimulation] Starting simulation!")
        prices = self.client.getAllPrices()
        self.fakeWallet.updateBUSD(prices)
        startingValue = sum(list(self.fakeWallet.getBUSDValue().values()))
        uniqueCoins = set(list(self.fakeWallet.balance.keys()))
        self.fakeWallet.printSummary(showCoins=True)
        history = [startingValue]

        while True:
            self.timestep += 1
            dlog(1, "[BinanceSimulation] Timestep: %d" % self.timestep)

            prices = self.client.getAllPrices()
            actions = self.bot.makeNewActions(self.fakeWallet.balance, prices)
            for action in actions:
                uniqueCoins.add(action[1]) if not action[1] is None else []
                self.doAction(action, prices)

            # Update BUSD & print summary.
            self.fakeWallet.updateBUSD(prices)
            self.fakeWallet.printSummary()
            history.append(sum(list(self.fakeWallet.getBUSDValue().values())))
            time.sleep(self.lag)

            if self.timestep % 10 == 0 and not self.plotFilePath is None:
                self.plotHistory(history)

            if self.timestep == self.endTime:
                break

        dlog(1, "[BinanceSimulation] Reached timestep=%d. Ending simulation!" % self.timestep)
        endValue = sum(list(self.fakeWallet.getBUSDValue().values()))
        dlog(1, "[BinanceSimulation] Starting BUSD: %2.5f. Ending BUSD: %2.5f. Diff: %2.5f" % \
            (startingValue, endValue, endValue - startingValue))
        dlog(1, "[BinanceSimulation] Shuffled through %d coins: %s" % (len(uniqueCoins), uniqueCoins))

    def plotHistory(self, history):
        plt.clf()
        plt.plot(range(len(history)), len(history) * [history[0]], color="red")
        plt.plot(range(len(history)), history, color="blue")
        plt.xlabel("Timestep")
        plt.ylabel("BUSD")
        plt.suptitle("Bot evolution")
        plt.savefig(self.plotFilePath)

    def __str__(self):
        Str = "[BinanceSimulation]"
        Str += "\n - End time: %ds" % self.endTime
        Str += "\n - Lag: %2.2fs" % self.lag
        Str += "\n - Plot file path: %s" % self.plotFilePath
        Str += "\n%s" % self.bot
        return Str