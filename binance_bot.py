from abc import abstractmethod
from binance_requests import BinanceClient
from typing import List, Dict, Tuple

ActionResult = List[Tuple[str, str, str, float, float]]
Balance = Dict[str, Tuple[float, float]]
Prices = Dict[str, float]

class BinanceBot:
    def __init__(self, apiFilePath:str):
        self.apiFilePath = apiFilePath
        self.client = BinanceClient(secretFilePath=apiFilePath)

    def sellAll(self, balance:Balance):
        res = []
        for coin in balance:
            if coin == "BUSD":
                continue
            item = ("SELL", coin, "BUSD", balance[coin][0], None)
            assert balance[coin][0] > 0
            res.append(item)
        return res

    def makeNewActions(self, balance:Balance, prices:Prices) -> ActionResult:
        return [("PASS", None, None, None, None)]

    def __str__(self) -> str:
        return "[Binance Bot] - Generic bot"